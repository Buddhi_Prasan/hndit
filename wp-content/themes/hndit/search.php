<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package HNDIT
 */

get_header(); ?>

<section class="feature-image feature-image-default" data-type="background" data-speed="1" style="background-position: 50% 0px;">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        	<h2 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'hndit' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
      </div>
    </div>
  </div>
</section>

<!-- Page Content -->
<div class="container">
  <div class="row">
		<!-- Blog Entries Column -->
		<div class="col-md-9">

		<?php
		if ( have_posts() ) : ?>

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

	</div>
	<div class="col-md-3">
		<?php get_sidebar(); ?>
	</div>
</div>
</div>

<?php

get_footer();
