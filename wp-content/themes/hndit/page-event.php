<?php
/* template Name: event page*/
//cusomer fildes
//advans customer fildes
get_header();
?>
<?php get_template_part( 'template-parts/content', 'titel' ); ?>

<div class="event">
  <div class="container">
    <div class="row">

      <?php $loop = new WP_Query( array( 'post_type' => 'event_gallery',
      'orderby' => 'post_id', 'order' => 'ASC' ) ); ?>
      <?php while( $loop->have_posts() ) : $loop->the_post(); ?>

            <div class="col-md-8 col-md-offset-2">
              <h2 class="ScrollReveal8 topic_style"><?php the_title(); ?></h2>
              <p class="ScrollReveal9">
                <?php the_content(); ?>
              </p>
            </div>
            <div class="col-md-12">
              <div class="gallery ScrollReveal10">
                <!-- gallery -->
              </div>
              <hr>

            <?php endwhile; ?>

        <!-- Pager -->
        <nav aria-label="Page navigation">
          <ul class="pagination">
            <li>
              <a href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
              </a>
            </li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li>
              <a href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
              </a>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
