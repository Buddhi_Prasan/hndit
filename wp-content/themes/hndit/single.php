<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package HNDIT
 */

get_header(); ?>

<section class="feature-image feature-image-default" data-type="background" data-speed="1" style="background-position: 50% 0px;">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <h2 class="ScrollReveal5"><?php single_post_title(); ?></h2>
      </div>
    </div>
  </div>
</section>

<!-- Page Content -->
<div class="container" id="blog_page">
	<div class="row">
		<!-- Blog Post Content Column -->
		<div class="col-lg-9">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_format() );


			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

</div>

<div class="col-md-3">
	<?php get_sidebar(); ?>
</div>

</div>
</div>


<?php
get_footer();
