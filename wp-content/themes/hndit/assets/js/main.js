
$(function() {

	// Cache the Window object
	var $window = $(window);

	//prallax background effect
	$('section[data-type="background"]').each(function(){
		var $bgobj = $(this); //assigning the object

		$(window).scroll(function() {

			//scroll the background at var speed
			// the yPos is a nagative valu because we're scrollingit UP!

			var yPos = -($window.scrollTop() / $bgobj.data('speed'));

			//put together our final background possition
			var coords = '50% '+ yPos + 'px' ;

			//move the background
			$bgobj.css({ backgroundPosition: coords });

		}); //end window scroll
	});
});



window.sr = new ScrollReveal();

// comman
window.sr.reveal('.ScrollReveal5', { duration: 500 });
window.sr.reveal('.ScrollReveal6', { duration: 600 });
window.sr.reveal('.ScrollReveal7', { duration: 700 });
window.sr.reveal('.ScrollReveal8', { duration: 800 });
window.sr.reveal('.ScrollReveal9', { duration: 900 });
window.sr.reveal('.ScrollReveal10', { duration: 1000 });
window.sr.reveal('.ScrollReveal11', { duration: 1100 });
window.sr.reveal('.ScrollReveal12', { duration: 1200 });
window.sr.reveal('.ScrollReveal13', { duration: 1300 });
window.sr.reveal('.ScrollReveal14', { duration: 1400 });
window.sr.reveal('.ScrollReveal15', { duration: 1500 });
