<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package HNDIT
 */

get_header(); ?>



		<?php
		if ( have_posts() ) : ?>

		<section class="feature-image feature-image-default" data-type="background" data-speed="1" style="background-position: 50% 0px;">
		  <div class="container">
		    <div class="row">
		      <div class="col-md-10 col-md-offset-1">
						<?php
							the_archive_title( '<h2 class="page-title">', '</h2>' );
							the_archive_description( '<div class="archive-description">', '</div>' );
						?>
		      </div>
		    </div>
		  </div>
		</section>

		<div class="container">
		  <div class="row">
				<!-- Blog Entries Column -->
				<div class="col-md-9">

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

	</div>
	<div class="col-md-3">
		<?php get_sidebar(); ?>
	</div>
</div>
</div>


<?php

get_footer();
