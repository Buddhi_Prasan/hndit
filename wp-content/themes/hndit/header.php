<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package HNDIT.LK
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Higher National Diploma in Information Technology (HNDIT) Exemptions from the other academic or Professional program : B. Sc. in Software Engineering and Information Technology – Open University of Sri Lanka Two year exemption from University of Moratuwa BIT program. B. Eng. in University of Vocational Technology Degree in City and Guilds 2 Year exemptions for B. Sc. in Software Engineering – Birmingham City University of UK With HNDIT and 2 Years Experience you can register for a M. Sc. degree.">
<meta name="keywords" content="hndit, HNDIT, Higher National Diploma in Information Technology ,SLIATE , Ministry of Higher Education ,Sri Lanka Institute of Advanced Technological Education, Sri Lanka, Information Technology, Information Systems, Computer Systems &amp; Networking, Business Management, Electronic Engineering, BSc, BIT, MSc, IT, Microsoft IT Academy,Cisco Networking Academy, CCNA, Sun Solaris, Graphics Design &amp; Multimedia, Linux, PHP, MySQL,System Analysis and Design,Programming Concepts,Database Design Concepts,Networking Concepts,Personnel Skills Development,Quality IT Systems,Data Analysis and Design,Management in IT Environment,End-User Support,Project Management in IT,Computer System and Operations,Software Engineering Concepts,Project /Industrial Training,HTML,CSS,XML,JavaScript">
<meta name="author" content="HNDIT Labuduwa Students">
<!-- Twitter Card data -->
<meta name="twitter:card" value="Higher National Diploma in Information Technology - Labuduwa">
 <!-- Open Graph data -->
<meta property="og:title" content="Higher National Diploma in Information Technology - Labuduwa" />
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
<!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<style media="screen">
html {
  margin-top: 0!important;
}
</style>

</head>

<body <?php body_class(); ?>>
  <header data-type="background" data-speed="1">
       <!-- <section id="page_top">
           <div class="container">
               <div class="row">
                   <div class="col-md-12">
                       <h6>Higher National Diploma in Information Technology - Labuduwa</h6>
                       <span>
                       <ul class=" navbar-nav navbar-right nav_top">
                         <li class="header_boder"><a href="#">Login</a></li>
                         <li><a href="#">Register</a></li>
                       </ul>
                     </span>
                   </div>
               </div>
           </div>
       </section> -->



    <nav class="navbar" role="navigation" data-spy="affix" data-offset-top="197">
        <div class="container">
    <div class="row">
        <div class="col-md-3 col-sm-12 cl-xs-12">
              <div class="navbar-header">
                  <a type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="icon-menu"></i><i class="fa fa-bars" aria-hidden="true"></i>
                  </a>
                  <a class="logo-l" href="#"><img src="<?php bloginfo('stylesheet_directory');?>/assets/img/logo.png" alt=""></a>
                  <a class="logo-lm" href="#"><img src="<?php bloginfo('stylesheet_directory');?>/assets/img/logo1.png" alt=""></a>
              </div>
            </div>

      <div class="col-md-9 col-sm-12 col-xs-12">
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="container1  circleBehind collapse navbar-collapse" id="main-navbar">
            <ul class="nav navbar-nav navbar-right nav_m">

                <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                <li>
                    <a href="<?php echo get_permalink( get_page_by_title( 'HOME' ) ); ?>"><i class="fa fa-home"></i></a>
                </li>
                <li>
                    <a href="<?php echo get_permalink( get_page_by_title( 'ABOUT' ) ); ?>"><i class="fa fa-university"></i></a>
                </li>
                <li>
                    <a href="<?php echo get_permalink( get_page_by_title( 'DIPLOMA' ) ); ?>"><i class="fa  fa-graduation-cap"></i></a>
                </li>
                <li>
                    <a href="<?php echo get_permalink( get_page_by_title( 'EVENT' ) ); ?>"><i class="fa  fa-camera"></i></a>
                </li>
                <li>
                    <a href="<?php echo get_permalink( get_page_by_title( 'BLOG' ) ); ?>"><i class="fa fa-file-text-o"></i></a>
                </li>
                <li>
                    <a href="<?php echo get_permalink( get_page_by_title( 'CONTACT' ) ); ?>"><i class="fa fa-address-card-o"></i></a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->


          </div>
          <!-- /.container -->
      </nav>
  </div>
 </header>
