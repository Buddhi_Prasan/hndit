    <div class="container">
        <div class="row">

            <section class="leftside">
                <div class="col-md-7 col-md-offset-1 dip">
                    <h2 class="ScrollReveal5">What is Diploma</h2>
                    <p class="dip_in ScrollReveal6">
                        HNDIT program at SLIATE is aimed to prepare students to enter industry as middle level IT professional required for the new millennium. This program provides a broad exposure compiled with a sound theoretical &amp; practical knowledge along with the skills
                        and positive attitudes.
                    </p>
                    <div class="list-group ScrollReveal7">
                        <h5 class="list-group-item-heading">Course Objective</h5>
                        <p class="list-group-item-text">At the end of the diploma the students should be able to
                        </p>
                    </div>

                    <div class="list-group ScrollReveal8">
                        <h5 class="list-group-item-heading">Function as a software developer</h5>
                        <p class="list-group-item-text">Use IT skills in the area of automation</p>
                        <p class="list-group-item-text">Make use of IT skills in decision making in an organization</p>
                        <p class="list-group-item-text">Train the personals in IT skills</p>
                    </div>

                    <div class="list-group ScrollReveal9">
                        <h5 class="list-group-item-heading">Course Objective</h5>
                        <p class="list-group-item-text">HNDIT (Full Time) – 2 ½Years</p>
                        <p class="list-group-item-text">HNDIT (Part Time) – 02 Years</p>
                    </div>
                    <a href="<?php echo esc_url( get_permalink( get_page_by_title( 'DIPLOMA' ) ) ); ?>" class="btn btn-danger btn-block ScrollReveal10">HNDIT Complete Syllabus</a>
                </div>

            </section>




            <aside class="rightside">
                <div class="col-md-4">
                    <h6 class="ScrollReveal5">LATEST NEWS</h6>

                      <?php $loop = new WP_Query( array( 'post_type' => 'news',
                      'orderby' => 'post_id', 'order' => 'ASC' ) ); ?>
                      <?php while( $loop->have_posts() ) : $loop->the_post(); ?>
                    <div class="media ScrollReveal6">
                        <div class="media-left media-middle">
                          <h4><?php the_field('end_date'); ?>
                          <small><?php the_field('month'); ?></small>
                          <?php the_field('year'); ?></h4>
                        </div>
                        <div class="media-body">
                            <h5 class="media-heading"><?php the_title(); ?></h5>
                            <p><?php the_content(); ?></p>
                        </div>
                    </div>
                      <hr>
                    <?php endwhile; ?>


                    <h6 class="ScrollReveal5">LATEST EVENT</h6>

                    <?php $loop = new WP_Query( array( 'post_type' => 'evnt',
                    'orderby' => 'post_id', 'order' => 'ASC' ) ); ?>
                    <?php while( $loop->have_posts() ) : $loop->the_post(); ?>
                  <div class="media ScrollReveal6">
                      <div class="media-left media-middle">
                        <h4><?php the_field('end_date'); ?>
                        <small><?php the_field('month'); ?></small>
                        <?php the_field('year'); ?></h4>
                      </div>
                      <div class="media-body">
                          <h5 class="media-heading"><?php the_title(); ?></h5>
                          <p><?php the_content(); ?></p>
                      </div>
                  </div>
                    <hr>
                  <?php endwhile; ?>

                </div>
            </aside>
        </div>
    </div>
