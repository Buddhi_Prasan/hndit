
<!-- slider  -->
<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1 hidden-xs">
      <div class="slider">
              <h3>HNDIT - LABUDUWA</h3>
              <hr>
              <p>
                Higher National Diploma in Information Technology (HNDIT) - SLIATE
              </p>
              <button type="button" class="btn btn-danger " data-toggle="modal" data-target="#myModal">Apply Now - 2017</button>
      </div>
      <!-- slider end -->
    </div>
  </div>
</div>


    <!-- top icon -->
    <section class="top_icon">
        <div class="container">
          <div class="row">
            <main>
                <div class="box red ScrollReveal5">
                    <i class="fa  fa-check-square-o"></i>
                    <span class="text">APPLY</span>
                </div>

                <div class="box black ScrollReveal8">
                    <i class="fa fa-book"></i>
                    <span class="text">COURSE</span>
                </div>

                <div class="box green ScrollReveal11">
                    <i class="fa fa-code"></i>
                    <span class="text">TRAINING</span>
                </div>

                <div class="box yello ScrollReveal14">
                    <i class="fa fa-graduation-cap"></i>
                    <span class="text">DIPLOMA</span>
                </div>
            </main>
          </div>

        </div>
    </section>
    <!-- top icon end -->
