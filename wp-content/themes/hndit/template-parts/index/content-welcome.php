
<?php
$welcome_description           = get_field('welcome_description');
 ?>


<!-- welcome -->
<section class="welcome">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 ">
                <h1 class="ScrollReveal5">WELCOME TO HNDIT - LABUDUWA</h1>
                <h3 class="ScrollReveal6">Higher National Diploma in Information Technology</h3>
                <p class="ScrollReveal7"><?php echo $welcome_description; ?></p>
            </div>

            <div class="col-md-6 col-md-offset-3">
                <button type="button" class="btn btn-danger btn-block ScrollReveal8" data-toggle="modal" data-target="#myModal">Apply Now - 2017</button>
            </div>
        </div>
    </div>
</section>
<!-- end welcome -->
