<?php
$youtube_video_link          = get_field('youtube_video_link');
 ?>

<section class="youtube">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 sub_btn ">
                <h1 class="ScrollReveal5">HNDIT - 2015 REVIEW</h1>
                <div class="embed-responsive embed-responsive-16by9">
                  <iframe class="embed-responsive-item ScrollReveal6" src="https://www.youtube.com/embed/8dHoghZYkeE"></iframe>
              </div>
                <a href="https://www.youtube.com/channel/UClQvJQcAvzuxnPBduJdZDKg" target="_blank" type="button" class="btn btn-danger btn-block ScrollReveal7">Subscribe our youtube channel</a>
            </div>
        </div>
    </div>
</section>
