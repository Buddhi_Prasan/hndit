
<?php
$mehe_description           = get_field('mehe_description');
$sliati_description           = get_field('sliati_description');
 ?>

<section class="gov">
    <div class="container">
        <div class="col-md-8 col-md-offset-2 ">
            <div class="row">
                <div class="mehe">
                    <img src="<?php bloginfo('stylesheet_directory');?>/assets/img/govlog1.png" class="img-responsive ScrollReveal5" alt="MEHE" />
                    <p class="gov_p ScrollReveal7"><?php echo $mehe_description; ?> </p>
                    <main class="cl-effect-4 ScrollReveal7">
                        <a href="http://www.mohe.gov.lk/">http://www.mohe.gov.lk/</a>
                    </main>
                </div>
            </div>
            <div class="row">
                <div class="mehe">
                    <img src="<?php bloginfo('stylesheet_directory');?>/assets/img/govlog2.png" class="img-responsive ScrollReveal5" alt="SLIATE" />
                    <p class="gov_p ScrollReveal7"><?php echo $sliati_description; ?> </p>
                    <main class="cl-effect-4 ScrollReveal8">
                        <a href="http://www.sliate.ac.lk/">http://www.sliate.ac.lk/</a>
                    </main>
                </div>
            </div>
        </div>
    </div>
</section>
