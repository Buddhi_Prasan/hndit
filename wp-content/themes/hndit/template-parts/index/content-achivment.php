<!-- achivment -->
<section class="achivment">
    <div class=" container">
        <div class="row">
            <h2 class="ScrollReveal5">ACHIVEMENT - 2015</h2>
            <main>
              <?php $loop = new WP_Query( array( 'post_type' => 'achivement',
              'orderby' => 'post_id', 'order' => 'ASC' ) ); ?>
              <?php while( $loop->have_posts() ) : $loop->the_post(); ?>
                <div class="col-sm-6  col-md-3">
                     <?php
                     $student_image  = get_field('student_image');
                      ?>
                    <div class="achivment_box ScrollReveal6">
                        <h3><?php the_title(); ?></h3>
                        <img src="<?php echo $student_image[url]; ?>" alt="<?php echo $student_image[alt]; ?>" />
                        <h4><?php the_field('student__name'); ?></h4>
                        <p><?php the_content(); ?></p>
                        <h4><?php the_field('event_1'); ?></h4>
                        <h5><?php the_field('1st_event_place'); ?></h5>
                        <h4><?php the_field('event_2'); ?></h4>
                        <h5><?php the_field('2nd_event_place'); ?></h5>
                    </div>
                </div>
                  <?php endwhile; ?>
            </main>
        </div>
    </div>
</section>
<!-- end achivment -->
