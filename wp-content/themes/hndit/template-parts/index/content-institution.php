
    <section class="institution">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading1 ">
                                <h4 class="panel-title ScrollReveal5">
                            <a data-toggle="collapse" data-parent="#accordion"
                            href="#collapseOne" class="btn btn-danger">Open University of Sri Lanka</a>
                        </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <div class="media">
                                        <div class="media-left media-middle">
                                            <img src="<?php bloginfo('stylesheet_directory');?>/assets/img/sliate.png" alt="SLIATE" />
                                        </div>
                                        <div class="media-body">
                                            <p>
                                                B. Sc. in Software Engineering and Information Technology
                                            </p>
                                            <main class="cl-effect-4">
                                                <a href="http://www.ou.ac.lk/home/">http://www.ou.ac.lk</a>
                                            </main>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading1">
                                <h4 class="panel-title ScrollReveal8">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="btn btn-danger">
                              University of Moratuwa </a>
                          </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="media">
                                        <div class="media-left media-middle">
                                            <img src="<?php bloginfo('stylesheet_directory');?>/assets/img/moratuwa.png" alt="MRT" />
                                        </div>
                                        <div class="media-body">
                                            <p>
                                                Two year exemption from University of Moratuwa BIT program
                                              </p>
                                            <main class="cl-effect-4">
                                                <a href="https://www.mrt.ac.lk/web/">https://www.mrt.ac.lk</a>
                                            </main>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading1">
                                <h4 class="panel-title ScrollReveal10">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapsethree" class="btn btn-success">
                              University of Vocational Technology</a>
                          </h4>

                            </div>
                            <div id="collapsethree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="media">
                                        <div class="media-left media-middle">
                                            <img src="<?php bloginfo('stylesheet_directory');?>/assets/img/univertec.png" alt="SLIATE" />
                                        </div>
                                        <div class="media-body">
                                            <p>
                                                B. Eng. in University of Vocational Technology
                                            </p>
                                            <main class="cl-effect-4">
                                                <a href="http://univotec.ac.lk/">http://univotec.ac.lk</a>
                                            </main>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading1">
                                <h4 class="panel-title ScrollReveal12">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour" class="btn btn-danger">
                              City and Guilds</a>
                          </h4>

                            </div>
                            <div id="collapsefour" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="media">
                                        <div class="media-left media-middle">
                                            <img src="<?php bloginfo('stylesheet_directory');?>/assets/img/city.png" alt="SLIATE" />
                                        </div>
                                        <div class="media-body">
                                            <p>
                                                Degree in City and Guilds
                                            </p>
                                            <main class="cl-effect-4">
                                                <a href="http://www.cityandguilds.com/">http://www.cityandguilds.com</a>
                                            </main>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading1">
                                <h4 class="panel-title ScrollReveal15">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapsefive" class="btn btn-danger">
                              Birmingham City University of UK</a>
                          </h4>

                            </div>
                            <div id="collapsefive" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <div class="media">
                                        <div class="media-left media-middle">
                                            <img src="<?php bloginfo('stylesheet_directory');?>/assets/img/brit.png" alt="SLIATE" />
                                        </div>
                                        <div class="media-body">
                                            <p>
                                                2 Year exemptions for B. Sc. in Software Engineering – Birmingham City University of UK
                                            </p>
                                            <main class="cl-effect-4">
                                                <a href="http://www.bcu.ac.uk/">http://www.bcu.ac.uk</a>
                                            </main>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
