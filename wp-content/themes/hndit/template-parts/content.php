<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package HNDIT.LK
 */

?>

<?php
$blog_writer             = get_field('blog_writer');
$blog_write_link         = get_field('blog_write_link');
$blog_write_position     = get_field('blog_write_position');
?>


 <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
   <div class="blog" id="blog">
 	<header class="entry-header">
 		<?php
 		if ( is_single() ) :
 			the_title( '<h1 class="blog_titel">', '</h1>' );
 		else :
 			the_title( '<h2 class="blog_titel"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
 		endif;

 		if ( 'post' === get_post_type() ) : ?>
    <hr class="titel_hr">
    <!-- Author -->
    <div class="blog_au">
      <!-- Author img-->
      <div class="blog_au_i">
        <?php
          $blog_write_image = get_field('blog_write_image');
          if( !empty($blog_write_image) ): ?>
          	<img src="<?php echo $blog_write_image['url']; ?>" alt="<?php echo $blog_write_image['alt']; ?>" />
        <?php endif; ?>
      </div>
      <!-- end Author img-->
      <!-- Author datils-->
      <div class="blog_au_n">
        <!-- Author name-->
        <p class="lead">by <a href="<?php echo $blog_write_link; ?>"> <?php echo $blog_writer; ?></a></p>
        <!-- end Author name-->
        <!--  Author about-->
        <p><i class="fa fa-user"></i> <strong>I'm</strong> <?php echo $blog_write_position; ?> |
          <i class="fa fa-clock-o"></i><time> <?php the_date(); ?></time></p>
        <!-- end Author about-->
      </div>
    </div>
    <!-- end  Author -->
 		<?php
 		endif; ?>
 	</header><!-- .entry-header -->

  <?php if ( has_post_thumbnail() ) { //cheack for future image  ?>
  		<div class="blog_body_i">
  			<?php the_post_thumbnail(); ?>
  		</div><!-- post-image -->
  		<?php } ?>

    <div class="blog_body">
 			<?php if (is_single()) : ?>
 					<?php the_content();  ?>
 			<?php else : ?>
 			<?php the_excerpt(); ?>
 			<?php endif; ?>
     </div>
 </article><!-- #post-## -->
