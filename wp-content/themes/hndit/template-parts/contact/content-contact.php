
<?php
$address          = get_field('address');
$email            = get_field('email');
$_hotline_number  = get_field('_hotline_number');
$fax_number	      = get_field('fax_number');
 ?>


<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-1">
                <form class="form-horizontal">
                  <?php echo do_shortcode( '[contact-form-7 id="48" title="Contact page"]' ); ?>
                </form>
            </div>

            <div class="col-md-4" id="contact_list">
                <ul class="ScrollReveal8">
                    <li class="con_l"><i class="fa  fa-map-marker"></i> Address</li>
                    <ul class="ScrollReveal9">
                        <li><address class="address">
                        <?php echo $address; ?>
                      </address></li>
                    </ul>
                </ul>

                <ul class="ScrollReveal8">
                    <li  class="con_l"><i class="fa fa-envelope"></i> Email</li>
                    <ul class="ScrollReveal9">
                        <li>  <?php echo $email; ?></li>
                    </ul>
                </ul>

                <ul class="ScrollReveal8">
                    <li  class="con_l"> <i class="fa fa-phone-square"></i> Hotline Number</li>
                    <ul class="ScrollReveal9">
                        <li>  <?php echo $_hotline_number; ?></li>
                    </ul>

                </ul>
                <ul class="ScrollReveal8">
                    <li  class="con_l"> <i class="fa fa-fax"></i> Fax Number</li>
                    <ul class="ScrollReveal9">
                        <li>  <?php echo $fax_number; ?></li>
                    </ul>

                </ul>
            </div>

        </div>
    </div>
</section>
