<?php $loop = new WP_Query( array( 'post_type' => 'event_gallery',
'orderby' => 'post_id', 'order' => 'ASC' ) ); ?>
<?php while( $loop->have_posts() ) : $loop->the_post(); ?>

      <div class="col-md-8 col-md-offset-2">
        <h2 class="ScrollReveal8 topic_style"><?php the_title(); ?></h2>
        <p class="ScrollReveal9">
          <?php the_content(); ?>
        </p>
      </div>
      <div class="col-md-12">
        <div class="gallery ScrollReveal10">
          <!-- gallery -->
          <img src="assets/img/gallery.png" class="img-responsive" alt="">
        </div>
        <hr>

      <?php endwhile; ?>
