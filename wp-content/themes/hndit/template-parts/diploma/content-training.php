<section class="dip_page1">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2 class="topic_style ScrollReveal5"> 6 Months Training Program </h2>
                <p class="ScrollReveal8">
                    After the completion of 2 year academic program you will be placed at a software development organization to follow industrial training under NAITA supervision for 6 months
                </p>
            </div>
        </div>
    </div>
</section>
