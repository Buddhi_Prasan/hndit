
<?php
$apply_title                 = get_field('apply_title');
$apply_button_name           = get_field('apply_button_name');
?>
<section class="dip_page1">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2 class="topic_style ScrollReveal7"><?php echo $apply_title; ?></h2>
                <p class="ScrollReveal8">
                  <?php while ( have_posts() ) : the_post(); ?>
                      <?php the_content(); ?>
                    <?php endwhile; //end of the loop ?></p>

                <button type="button" class="btn btn-danger ScrollReveal10 " data-toggle="modal" data-target="#myModal"><?php echo $apply_button_name; ?></button>

                <h2 class="ScrollReveal11">The Selection Criteria</h2>
                <p class="ScrollReveal12">
                    Candidates are selected through the order of merit in their GCE (A/L) examination and through an aptitude/ IQ test.
                </p>
            </div>
        </div>
    </div>
</section>
