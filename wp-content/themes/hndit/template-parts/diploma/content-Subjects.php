<section class="Subjects">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2 class="topic_style ScrollReveal5">Subjects and Credits</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12 " id="no-more-tables">
                <table class="table cf ScrollReveal8">
                    <h4 class="table_topc ScrollReveal7"> 1<sup>st</sup> Year - 1<sup>st</sup> Semester</h4>
                    <thead class="cf">
                        <tr>
                            <th class="numeric">Module Code</th>
                            <th class="numeric">Module Title</th>
                            <th class="numeric">Module Type</th>
                            <th class="numeric">Credits</th>
                            <th class="numeric">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-title="Module Code">HNDIT11012</td>
                            <td data-title="Module Title">Personal Computer Applications</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT11023</td>
                            <td data-title="Module Title">Computer Hardware</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT11034</td>
                            <td data-title="Module Title">Structured Programming</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">04</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT11042</td>
                            <td data-title="Module Title">Data Representation and Organization</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT11052</td>
                            <td data-title="Module Title">Database Management Systems</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT11062</td>
                            <td data-title="Module Title">Web Development</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT11072</td>
                            <td data-title="Module Title">Mathematics for Computing</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT11081</td>
                            <td data-title="Module Title">Communication Skills I</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">01</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <th class="hidden-xs hidden-sm"></th>
                            <th class="hidden-xs hidden-sm">TOTAL</th>
                            <td class="hidden-xs hidden-sm"></td>
                            <td>18</td>
                            <td class="hidden-xs hidden-sm"></td>
                        </tr>
                    </tbody>
                </table>
                <table class="table ScrollReveal8">
                    <h4 class="table_topc ScrollReveal7"> 1<sup>st</sup> Year - 2<sup>nd</sup> Semester</h4>
                    <thead>
                        <tr>
                            <th>Module Code</th>
                            <th>Module Title</th>
                            <th>Module Type</th>
                            <th>Credits</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-title="Module Code">HNDIT12094</td>
                            <td data-title="Module Title">Object Oriented Programming</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">04</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT12103</td>
                            <td data-title="Module Title">Graphics and Multimedia</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT12112</td>
                            <td data-title="Module Title">Data Structures and Algorithms</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT12123</td>
                            <td data-title="Module Title">Systems Analysis and Design</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT12133</td>
                            <td data-title="Module Title">Data Communications and Networks</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT12142</td>
                            <td data-title="Module Title">Statistics for IT</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT12151</td>
                            <td data-title="Module Title">Communication Skills II</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">01</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT12162</td>
                            <td data-title="Module Title">Leadership Skills and Development</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <th class="hidden-xs hidden-sm"></th>
                            <th class="hidden-xs hidden-sm">TOTAL</th>
                            <td class="hidden-xs hidden-sm"></td>
                            <td>20</td>
                            <td class="hidden-xs hidden-sm"></td>
                        </tr>
                    </tbody>
                </table>

                <!-- 2nd Year - 1st Semester -->
                <table class="table ScrollReveal8">
                    <h4 class="table_topc ScrollReveal7"> 2<sup>nd</sup> Year - 1<sup>st</sup> Semester</h4>
                    <thead>
                        <tr>
                            <th>Module Code</th>
                            <th>Module Title</th>
                            <th>Module Type</th>
                            <th>Credits</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-title="Module Code">HNDIT23012</td>
                            <td data-title="Module Title">Operating Systems and Computer Security</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT23022</td>
                            <td data-title="Module Title">Project Management</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT23032</td>
                            <td data-title="Module Title">Principles of Management and Applied Economics</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT23042</td>
                            <td data-title="Module Title">Technical Writing</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT23051</td>
                            <td data-title="Module Title">Mini Project</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT12142</td>
                            <td data-title="Module Title">Statistics for IT</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">01</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT23061</td>
                            <td data-title="Module Title">Communication Skills III</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">01</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <th class="hidden-xs hidden-sm"></th>
                            <th class="hidden-xs hidden-sm">TOTAL</th>
                            <td class="hidden-xs hidden-sm"></td>
                            <td>10</td>
                            <td class="hidden-xs hidden-sm"></td>
                        </tr>
                    </tbody>
                </table>

                <!-- 2nd Year - 1st Semester Semester Developer Track Option -->
                <table class="table ScrollReveal8">
                    <h6 class="table_topc ScrollReveal7"> 2<sup>nd</sup> Year - 1<sup>st</sup> Semester Developer Track Option</h6>
                    <thead>
                        <tr>
                            <th>Module Code</th>
                            <th>Module Title</th>
                            <th>Module Type</th>
                            <th>Credits</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-title="Module Code">HNDIT23073</td>
                            <td data-title="Module Title">Rapid Application Development</td>
                            <td data-title="Module Type">Track Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT23082</td>
                            <td data-title="Module Title">Principals of Software Engineering</td>
                            <td data-title="Module Type">Track Elective</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT23093</td>
                            <td data-title="Module Title">Object Oriented Analysis and Design</td>
                            <td data-title="Module Type">Track Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>

                        <tr>
                            <th class="hidden-xs hidden-sm"></th>
                            <th class="hidden-xs hidden-sm">TOTAL</th>
                            <td data-title="TOTEL">Track Elective</td>
                            <td>08</td>
                            <td class="hidden-xs hidden-sm"></td>
                        </tr>
                    </tbody>
                </table>

                <!-- 2nd Year - 1st Semester Administrator Track Option -->
                <table class="table ScrollReveal8">
                    <h6 class="table_topc ScrollReveal7"> 2<sup>nd</sup> Year - 1<sup>st</sup> Administrator Track Option</h6>
                    <thead>
                        <tr>
                            <th>Module Code</th>
                            <th>Module Title</th>
                            <th>Module Type</th>
                            <th>Credits</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-title="Module Code">HNDIT23103</td>
                            <td data-title="Module Title">Advance Database Management Systems</td>
                            <td data-title="Module Type">Track Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT23122</td>
                            <td data-title="Module Title">InternetworkingTrack Elective</td>
                            <td data-title="Module Type">Track Elective</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT23133</td>
                            <td data-title="Module Title">Enterprise Information Security Systems</td>
                            <td data-title="Module Type">Track Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>

                        <tr>
                            <th class="hidden-xs hidden-sm"></th>
                            <th class="hidden-xs hidden-sm">TOTAL</th>
                            <td data-title="TOTEL">Track Elective</td>
                            <td>08</td>
                            <td class="hidden-xs hidden-sm"></td>
                        </tr>
                    </tbody>
                </table>

                <!-- 2nd Year - 1st Semester Analyst Track Option -->
                <table class="table ScrollReveal8">
                    <h6 class="table_topc ScrollReveal7"> 2<sup>nd</sup> Year - 1<sup>st</sup> Analyst Track Option</h6>
                    <thead>
                        <tr>
                            <th>Module Code</th>
                            <th>Module Title</th>
                            <th>Module Type</th>
                            <th>Credits</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-title="Module Code">HNDIT23142</td>
                            <td data-title="Module Title">Introduction to Business Analysis</td>
                            <td data-title="Module Type">Track Elective</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT23153</td>
                            <td data-title="Module Title">Management Information Systems</td>
                            <td data-title="Module Type">Track Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT23163</td>
                            <td data-title="Module Title">E-Commerce</td>
                            <td data-title="Module Type">Track Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>

                        <tr>
                            <th class="hidden-xs hidden-sm"></th>
                            <th class="hidden-xs hidden-sm">TOTAL</th>
                            <td data-title="TOTEL">Track Elective</td>
                            <td>08</td>
                            <td class="hidden-xs hidden-sm"></td>
                        </tr>
                    </tbody>
                </table>

                <!-- 2nd Year - 2nd Semester -->
                <table class="table ScrollReveal8">
                    <h4 class="table_topc ScrollReveal7"> 2<sup>nd</sup> Year - 2<sup>nd</sup> Semester</h4>
                    <thead>
                        <tr>
                            <th>Module Code</th>
                            <th>Module Title</th>
                            <th>Module Type</th>
                            <th>Credits</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-title="Module Code">HNDIT24012</td>
                            <td data-title="Module Title">Computer Architecture</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24022</td>
                            <td data-title="Module Title">Open Source Systems</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24032</td>
                            <td data-title="Module Title">IT and Society</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24044</td>
                            <td data-title="Module Title">Technical Writing</td>
                            <td data-title="Module Type">Project</td>
                            <td data-title="Credits">04</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24052</td>
                            <td data-title="Module Title">Communication Skills IV</td>
                            <td data-title="Module Type">Core</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <th class="hidden-xs hidden-sm"></th>
                            <th class="hidden-xs hidden-sm">TOTAL</th>
                            <td class="hidden-xs hidden-sm"></td>
                            <td>12</td>
                            <td class="hidden-xs hidden-sm"></td>
                        </tr>
                    </tbody>
                </table>

                <!-- 2nd Year - 2nd Semester Semester Developer Track Option -->
                <table class="table ScrollReveal8">
                    <h6 class="table_topc ScrollReveal7"> 2<sup>nd</sup> Year - 2<sup>nd</sup> Semester Developer Track Option</h6>
                    <thead>
                        <tr>
                            <th>Module Code</th>
                            <th>Module Title</th>
                            <th>Module Type</th>
                            <th>Credits</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-title="Module Code">HNDIT24113</td>
                            <td data-title="Module Title">Multi-tiered Application Development</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24123</td>
                            <td data-title="Module Title">Software Configuration Management</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24133</td>
                            <td data-title="Module Title">Web Programming</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24143</td>
                            <td data-title="Module Title">Computer Graphics and Animation Design</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24153</td>
                            <td data-title="Module Title">Digital Image Processing</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24163</td>
                            <td data-title="Module Title">Digital Video and Audio</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">02</td>
                            <td data-title="Status">GPA</td>
                        </tr>

                        <tr>
                            <th class="hidden-xs hidden-sm"></th>
                            <th class="hidden-xs hidden-sm">TOTAL CREDITS REQUIRED</th>
                            <td data-title="TOTAL CREDITS REQUIRED">Track Elective</td>
                            <td>09</td>
                            <td class="hidden-xs hidden-sm"></td>
                        </tr>
                    </tbody>
                </table>

                <!-- 2nd Year - 2nSemester Administrator Track Option -->
                <table class="table ScrollReveal8">
                    <h6 class="table_topc ScrollReveal7"> 2<sup>nd</sup> Year - 2<sup>nd</sup> Administrator Track Option</h6>
                    <thead>
                        <tr>
                            <th>Module Code</th>
                            <th>Module Title</th>
                            <th>Module Type</th>
                            <th>Credits</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-title="Module Code">HNDIT24213</td>
                            <td data-title="Module Title">Server Installation and Management</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24223</td>
                            <td data-title="Module Title">Network and Data Centre Operations</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24233</td>
                            <td data-title="Module Title">Disaster Recovery & Business Continuity Planning</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>

                        <tr>
                            <td data-title="Module Code">HNDIT24243</td>
                            <td data-title="Module Title">DB Server Installation and Management</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24253</td>
                            <td data-title="Module Title">Database Programming Project</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>

                        <tr>
                            <th class="hidden-xs hidden-sm"></th>
                            <th class="hidden-xs hidden-sm">TOTAL</th>
                            <td data-title="TOTEL">Track Elective</td>
                            <td>09</td>
                            <td class="hidden-xs hidden-sm"></td>
                        </tr>
                    </tbody>
                </table>

                <!-- 2nd Year - 2nd Semester Analyst Track Option -->
                <table class="table ScrollReveal8">
                    <h6 class="table_topc ScrollReveal7"> 2<sup>nd</sup> Year - 2<sup>nd</sup> Analyst Track Option</h6>
                    <thead>
                        <tr>
                            <th>Module Code</th>
                            <th>Module Title</th>
                            <th>Module Type</th>
                            <th>Credits</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-title="Module Code">HNDIT24313</td>
                            <td data-title="Module Title">Software Testing</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24323</td>
                            <td data-title="Module Title">Technical Report Writing</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24333</td>
                            <td data-title="Module Title">Software Quality Management</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24323</td>
                            <td data-title="Module Title">Technical Report Writing</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24343</td>
                            <td data-title="Module Title">Business Analysis - Tools & Processes</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24353</td>
                            <td data-title="Module Title">System Analysis Case Study</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>

                        <tr>
                            <th class="hidden-xs hidden-sm"></th>
                            <th class="hidden-xs hidden-sm">TOTAL</th>
                            <td data-title="TOTEL">Track Elective</td>
                            <td>09</td>
                            <td class="hidden-xs hidden-sm"></td>
                        </tr>
                    </tbody>
                </table>

                <!-- 2nd Year - 2nd Academic Track Option -->
                <table class="table ScrollReveal8">
                    <h6 class="table_topc ScrollReveal7"> 2<sup>nd</sup> Year - 2<sup>nd</sup> Academic Track Option</h6>
                    <thead>
                        <tr>
                            <th>Module Code</th>
                            <th>Module Title</th>
                            <th>Module Type</th>
                            <th>Credits</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-title="Module Code">HNDIT24413</td>
                            <td data-title="Module Title">Educational Psychology</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24423</td>
                            <td data-title="Module Title">Teaching Methodology</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>
                        <tr>
                            <td data-title="Module Code">HNDIT24433</td>
                            <td data-title="Module Title">Educational Measurement and Evaluation</td>
                            <td data-title="Module Type">Field Elective</td>
                            <td data-title="Credits">03</td>
                            <td data-title="Status">GPA</td>
                        </tr>

                        <tr>
                            <th class="hidden-xs hidden-sm"></th>
                            <th class="hidden-xs hidden-sm">TOTAL</th>
                            <td data-title="TOTEL">Track Elective</td>
                            <td>09</td>
                            <td class="hidden-xs hidden-sm"></td>
                        </tr>
                    </tbody>
                </table>


            </div>

        </div>

    </div>
</section>
