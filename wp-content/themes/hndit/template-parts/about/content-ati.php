<?php
$ati_discretion            = get_field('ati_discretion');
$director_name             = get_field('director_name');
$director_image            = get_field('director_image');
$ar_name                   = get_field('ar_name');
$ar_image                  = get_field('ar_image');
 ?>

<section class="gov">
    <div class="container">
        <div class="col-md-10 col-md-offset-1 ">
            <div class="row">
                <h2 class="ScrollReveal5 topic_style">Advanced Technological Institute - Galle</h2>
                <div class="mehe">
                    <img src="<?php bloginfo('stylesheet_directory');?>/assets/img/govlog2.png" class="img-responsive ScrollReveal6" alt="SLIATE" />
                    <p class="ScrollReveal7"><?php echo $ati_discretion; ?></p>
                    <main class="cl-effect-4 ScrollReveal8">
                        <a href="http://hndit.lk/">http://www.atigalle.lk</a>
                    </main>
                </div>
            </div>
        </div>
    </div>
</section>

    <section class=" director column hover14">
        <div class="container">
            <div class="row">
              <div class="col-md-4 col-sm-4 col-md-offset-2">
                  <figure>
                      <?php if ( !empty($director_image) ) : ?>
                        <img src="<?php  echo $director_image['url']; ?>" class="img-responsive ScrollReveal11"
                        alt="<?php  echo $director_image['alt']; ?>" />
                      <?php endif; ?>
                  </figure>

                  <h4><?php echo $director_name; ?><br><small>Directer</small></h4>

              </div>
              <hr class="lec1_hr">
              <div class="col-md-4 col-sm-4">
                <figure>
                    <?php if ( !empty($ar_image) ) : ?>
                      <img src="<?php  echo $ar_image['url']; ?>" class="img-responsive ScrollReveal11"
                      alt="<?php  echo $ar_image['alt']; ?>" />
                    <?php endif; ?>
                </figure>
                  <h4><?php echo $ar_name; ?><br><small>Registrar</small></h4>
              </div>
            </div>
        </div>
    </section>
