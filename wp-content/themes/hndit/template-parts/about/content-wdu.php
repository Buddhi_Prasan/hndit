<?php
$wdu_desricption            = get_field('wdu_desricption');
?>

<section class="gov">
    <div class="container">
        <div class="col-md-10 col-md-offset-1 ">
            <div class="row">
                <h2 class="ScrollReveal5 topic_style">HNDIT Web Devlepment Unit</h2>
                <div class="mehe">
                    <img src="<?php bloginfo('stylesheet_directory');?>/assets/img/hnditwdu.png" class="img-responsive ScrollReveal6" alt="wdu" />
                    <p class="ScrollReveal7">
                      <?php echo $wdu_desricption; ?>
                    </p>
                    <main class="cl-effect-4 ScrollReveal8">
                        <a href="http://hndit.lk/">http://www.hndit.lk</a>
                    </main>
                </div>
            </div>
        </div>
    </div>
</section>
