
<?php
$it_department_description            = get_field('it_department_description');
?>


<section class="gov">
    <div class="container">
        <div class="col-md-10 col-md-offset-1 ">
            <div class="row">
                <h2 class="ScrollReveal5 topic_style">Department Of Information Technology </h2>
                <div class="mehe">
                    <img src="<?php bloginfo('stylesheet_directory');?>/assets/img/logo.png" class="img-responsive ScrollReveal6" alt="SLIATE" />
                    <p><?php echo $it_department_description; ?></p>
                    <main class="cl-effect-4 ScrollReveal7">
                        <a href="http://hndit.lk/">http://www.hndit.lk</a>
                    </main>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="director column hover14">
    <div class="container">
        <div class="row">

          <?php $loop = new WP_Query( array( 'post_type' => 'lecturer',
          'orderby' => 'post_id', 'order' => 'ASC' ) ); ?>
          <?php while( $loop->have_posts() ) : $loop->the_post(); ?>
            <?php
            $student_image  = get_field('lecturer_image');
             ?>
            <div class="col-md-4 col-sm-4">
                <figure><img src="<?php echo $student_image[url]; ?>" class="<?php echo $student_image[alt]; ?>" alt="Director" /></figure>
                <h4><?php the_title(); ?><br><small><?php the_field('lecturer_position'); ?></h4></span>
            </div>
              <?php endwhile; ?>

    </div>
</section>
