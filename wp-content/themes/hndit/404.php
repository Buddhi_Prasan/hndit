<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package HNDIT
 */

get_header(); ?>


<section class="feature-image feature-image-default" data-type="background" data-speed="1" style="background-position: 50% 0px;">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <h2 class="ScrollReveal5">HNDIT - 404 Error</h2>
      </div>
    </div>
  </div>
</section>


<div class="container" >
        <div Class="row">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'hndit' ); ?></h1>
				</header><!-- .page-header -->

			
			</section><!-- .error-404 -->

					</div>
				</div><!-- #primary -->

<?php
get_footer();
