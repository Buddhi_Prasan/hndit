<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package HNDIT.LK
 */

?>

<footer id="footer">
		<div class="container">
				<div class="col-md-4">
						<div class="news">
								<h4>NEWS LETTER</h4>
								<p>
	Keep in touch with latest news by subcribing our newsletter.					</p>
							<?php echo do_shortcode( '[contact-form-7 id="160" title="Subscribe Email"]' ); ?>
						</div>
				</div>
				<div class="col-md-4">
						<div class="social">
								<h4>FACEBOOK</h4>
								<ul>
										<li><a href="https://www.facebook.com/hndit.labuduwa/" target="_blank"><i class="fa fa-facebook"></i></a></li>
										<li><a href="https://twitter.com/HNDIT_Labuduwa" target="_blank"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="https://www.youtube.com/channel/UClQvJQcAvzuxnPBduJdZDKg" target="_blank"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin" target="_blank"></i></a></li>
								</ul>
						</div>
				</div>
				<div class="col-md-4">
						<div class="news">
								<h4>CONTACT US</h4>
								<ul>
										<li class="main_f"><i class="fa fa-call"></i>Telephone</li>
										<ul>
												<li class="main_g">+94 91-224-6179 / +94 91-222-7880 </li>
										</ul>
										<li class="main_f"><i class="fa fa-letter"></i>E-Mail</li>
										<ul>
												<li class="main_g">info@hndit.lk</li>
										</ul>
								</ul>
						</div>
				</div>
				<div class="row Copyrights">
						<div class="col-md-12">
								<p>
										All Copyrights Reserved by HNDIT Web Development Unit - © 2017
								</p>

						</div>

				</div>

		</div>

</footer>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
				<div class="modal-content">
						<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
								<h4 class="modal-title" id="myModalLabel">APPLY 2017 HNDIT - LABUDUWA</h4>
								<p class="modal-title">
										Higher National Diploma in Information Technology
								</p>
						</div>
						<div class="modal-body">
						<?php echo do_shortcode( '[contact-form-7 id="199" title="HNDIT Apply 2017"]' ); ?>
						</div>

				</div>
		</div>
</div>



<script src="https://unpkg.com/scrollreveal@3.3.2/dist/scrollreveal.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

<!-- boostrap core javasript -->
<script src="<?php bloginfo('stylesheet_directory');?>/assets/js/bootstrap.min.js"></script>

<!-- jquvery  -->
<script src="<?php bloginfo('stylesheet_directory');?>/assets/js/jquery.js"></script>

<!-- custom javasript -->
<script src="<?php bloginfo('stylesheet_directory');?>/assets/js/main.js"></script>
<script>
function showSimpleDialog() {
  $( "#SimpleModalBox" ).modal();
}

function doSomethingBeforeClosing() {
  //Do something. For example, display a result:
  $( "#TheBodyContent" ).text( "Operation completed successfully" );

  //Close dialog in 3 seconds:
  setTimeout( function() { $( "#SimpleModalBox" ).modal( "hide" ) }, 3000 );
}
</script>
</body>
</html>
